import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NTupleComparison {
	
	private double percentMatch;	//	percent of tuples in file1 that appear in file2
	
	// Dictionary look up for a synonyms
	private Map<String, String> synDictionary = new HashMap<String, String>();
	
	// hash set of tuples from file two since the program doesn't care about number of tuples in second file
	private Set<String> file2Tuples = new HashSet<String>();					
	
	// get rid of special characters, especially # since I use it as a id for synonyms
	String delims = "[ !,?._@#$%&;:']+";
	
	protected NTupleComparison() {
		percentMatch = 0.0;
	}
	
	
	/**
	 * Function that performs plagiarism detection with given file of synonyms, two files and tuple size
	 * Steps:
	 * 1 - function checks if tuple size is not greater than any of two files that function compares
	 * 2 - create dictionary look up map for synonyms (more in a readMe)
	 * 3 - replace synonyms in both text with a synonym id
	 * 4 - create hash set of tuples for fast look up and the number of tuples in file doesn't not effect on plagiarism percentage
	 * 5 - lastly, function compares tuples and performs percentage calculation
	 * 	
	 * @param synonyms  	File that contains synonym list
	 * @param fileOne		Text of the first file in a string
	 * @param fileTwo		Text of the second file in a string
	 * @param tupleSize		Tuple size - default is 3
	 * 
	 * @return double 		percent of tuples in file1 that appear in file2
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public double plagiarismDetector(String synListFile, String fileOne, String fileTwo, int tupleSize) throws FileNotFoundException, IOException {
		
		// Perform check on text files 
		if (fileOne.split(delims).length < tupleSize ){
			System.out.println("First file is less than a tuple size: " + tupleSize);
			return percentMatch;
		}
		
		if (fileTwo.split(delims).length < tupleSize ){
			System.out.println("Second file is less than a tuple size: " + tupleSize);
			return percentMatch;
		}
		
		// get synonym dictionary
		synDictionary = synonymReplacement(synListFile);
		
		// replace synonyms in text with synonym id
		String fileOneText = replaceSynsWithTag(fileOne, synDictionary);
		String fileTwoText = replaceSynsWithTag(fileTwo, synDictionary);
		
		// put all tuples from text two to a hash set
		file2Tuples = textToTupleSet(fileTwoText, tupleSize);
		
		// calculate percentage of tuples that appear in file 2
		percentMatch = getTupleMatchPercentage(fileOneText, file2Tuples, tupleSize);
		
		return percentMatch;
	}
	
	
	/**
	 * Function that creates ids and puts them in HashMap for fast lookup
	 * HashMap<Synonym, SynonymID>
	 * 
	 * Assuming that synonyms are separated by one space and no special characters
	 * 
	 * @param 	synonymList 				files with a list of synonyms
	 * @return 	HashMap<String, String> 	dictionary id dictionary
	 * @throws 	FileNotFoundException
	 * @throws 	IOException
	 */
	public HashMap<String, String> synonymReplacement (String synonymList) throws FileNotFoundException, IOException{
		HashMap<String, String> dictionary = new HashMap<String, String>();
		int counter = 0;
		// hash tag and counter work as a unique identifier for a list of synonyms
		// so each word in a synonym list treated equally in the text (more in a readme)
		String tag = "#";
		try (BufferedReader br = new BufferedReader(new FileReader(synonymList))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	// Assuming synonym list is well formated and each word separated by only one space
		    	String[] synSet = line.toLowerCase().split(" ");
		    	String id = tag.concat(Integer.toString(counter)); 
		    	for (String s: synSet) {
		    		dictionary.put(s, id);
		    	}
		    	counter++;
		    }
		} catch(Exception e) {
			System.out.println("Please check you synonym file: " + synonymList);
			System.out.println("Exitting...");
			System.exit(1);
		}
		return dictionary;
	}
	
	/**
	 * Replaces text with synonym id from synonym dictionary
	 * 
	 * @param String 				text 	
	 * @param Map<String, String> 	synDictionary
	 */
	public String replaceSynsWithTag(String text, Map<String, String> synDictionary) {
		
		String[] wordsInText = text.toLowerCase().trim().split(" ");
		for (int i = 0; i < wordsInText.length; i++) {
			String word = wordsInText[i];
			if(synDictionary.containsKey(word)) {
				wordsInText[i] = synDictionary.get(word);
			}
		}
		StringBuilder newText = new StringBuilder();
		for(String word: wordsInText) {
			newText.append(word).append(" ");
		}
		return newText.toString().trim();
	}
	
	/**
	 * Creates Hash set of tuples from a given text
	 * 
	 * @param String 	text
	 * @param int 		tupleSize
	 * @return
	 */
	public Set<String> textToTupleSet (String text, int tupleSize) {
		Set<String> tupleSet = new HashSet<String>();
		String[] wordsInText = text.toLowerCase().trim().split(" ");;
		String tuple = "";
		
		for (int i = 0; i < tupleSize; i++ ) {
			tuple = tuple.concat(wordsInText[i]);
			tuple = tuple.concat(" ");
		}
		tuple = tuple.trim();
		tupleSet.add(tuple);

		for (int i = tupleSize; i < wordsInText.length; i++) {
			int start = tuple.indexOf(" ");
			tuple = tuple.substring(start + 1);
			tuple = tuple.concat(" " + wordsInText[i]);
			tupleSet.add(tuple);
		}
		
		return tupleSet;
	}
	
	/**
	 * Performs comparison between text and a set of tuples and 
	 * computes percentage of tuple matches from given text
	 * 
	 * @param String		text1
	 * @param Set<String>	file2Tuples2
	 * @param int			tupleSize
	 * @return
	 */
	public double getTupleMatchPercentage(String text1, Set<String> file2Tuples2, int tupleSize) {
		int matchCount = 0;
		int tupleCount = 0;
		String[] wordsInText = text1.toLowerCase().trim().split(" ");
		String tuple = "";

		for (int i = 0; i < tupleSize; i++ ) {
			tuple = tuple.concat(wordsInText[i] + " ");
		}
		tuple = tuple.trim();

		if(file2Tuples2.contains(tuple)) {
			matchCount++;
		}
		
		tupleCount++;
		for (int j = tupleSize; j < wordsInText.length; j++) {
			int start = tuple.indexOf(" ");
			tuple = tuple.substring(start + 1);
			tuple = tuple.concat(" " + wordsInText[j]);

			if(file2Tuples2.contains(tuple)) {
				matchCount++;
			}
			tupleCount++;
		}
		
		return calculatePercentage(tupleCount, matchCount);
	}
	
	/**
	 * Simple calculation for percentage
	 *  
	 * @param int	totalTuples
	 * @param int	matchedTuples
	 * @return
	 */
	public double calculatePercentage(int totalTuples, int matchedTuples) {
		return (100 * (double)matchedTuples)/(double) totalTuples;	
	}
}
