Name:			 Adilet Issayev	
Assignment:		 Plagiarism Checker
----------------------
Command Line Arguments
----------------------
This is a command-line program that perform plagiarism detection 
against two files using N-tuple comparison allowing synonyms 
in the text.

1. file name for a list of synonyms
2. input file 1
3. input file 2
4. (optional) the number N, the tuple size. If not supplied, the default should be N=3.


Usage 		java -jar plagChecker.jar <synonyms file> <first file> <second file> [tuple size]

Example:  	java -jar plagChecker.jar syns.txt file1.txt file2.txt 3
										
-------------------------
Program Notes
-------------------------

1. Both files can be empty
2. Both file can have total number of words less than a tuple size,
   in that case just return 0.0 for percentage
3. Program removes extra spaces and special characters
4. Program ignore upper cases

	SPECIAL NOTE:	program removes special characters specifically #
					because programs treats all synonyms as equals
					it creates a dictionary for synonym words associated with unique id
					Each synonym list will have the same id
					
					Example:
						run jog sprint 
						go went
						
					HashMap<String, String> m;
					m[run, #1]
					m[jog, #1
					m[sprint, #1]
					m[go, #2]
					m[went, #2]
					
					Then, program goes over both texts and replaces all words that are in synonymDictionary
					with an id. This improves efficiency on tuple comparison
					
					Before replacement:		1) go for a run		2) went for a jog
					After replacement:		   #2 for a #1		  #2 for a #1
					




