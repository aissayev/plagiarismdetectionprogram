import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class PlagiarismDetection {

	/**
	 * Main
	 * 
	 * @param  args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		String synonymsFile = null;
		String fileOne = null, fileTwo = null;
		int	tupleSize = 3;
		// input argument check
		if (args.length > 0) {
			if(args.length < 3 || args.length > 4) {
				printUsage();
				System.exit(-1);
			}
			
			synonymsFile = args[0];
			fileOne = args[1];
			fileTwo = args[2];
			if(args.length == 4) {
				if(isNumeric(args[3])) {
					String s  = args[3];
					tupleSize = Integer.parseInt(s);
					if (tupleSize < 3) {
						tupleSize = 3;
						System.out.println("Your tuple size is less than default... Running program with default tupleSize = 3");
					}
				} else {
					System.out.println("tuple size must be numeric integer... Exiting");
					System.exit(1);
				}

			}

		} else {
			printUsage();
			System.exit(-1);
		}
		
		
		
		String textOne = readFileToString(fileOne);
		String textTwo = readFileToString(fileTwo);
	
		
		NTupleComparison tc = new NTupleComparison();
		double output = tc.plagiarismDetector(synonymsFile, textOne, textTwo, tupleSize);
		
		System.out.println("Plagiarism percentage: " + output);
	
		
	}
	
	/**
	 * Print usage of the program
	 */
	public static void printUsage() {
		System.out.println("USAGE: <synonyms file> <first file> <second file> [tuple size]");
	}
	
	/**
	 * Read File into a String
	 * 
	 * @param 	fileName
	 * @return 	String 			text in a string 
	 * @throws 	IOException
	 */
	public static String readFileToString(String fileName) throws IOException, FileNotFoundException {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
		    
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		            sb.append(line);
		            sb.append(" ");			// >>> double check
		            line = br.readLine();
		        }
		        return sb.toString();
		    }  catch (Exception e) {
				System.out.println("Could not read a file : " + fileName);
				System.exit(1);
			} finally {
		        br.close();
		    }
		
			
		} catch (Exception e) {
			System.out.println("File not found: " + fileName);
			System.exit(1);
		}
		
		return fileName;

	}
	
	// function from stack overflow to check if string is Numeric
	public static boolean isNumeric(String str)
	  {
	    try
	    {
	      Integer.parseInt(str);
	    }
	    catch(NumberFormatException nfe)
	    {
	      return false;
	    }
	    return true;
	  }
	
	
	
	
	

}
